pluginManagement {
    // Jetbrains/Kotlin
    val kotlinVersion: String by settings
    // Compose
    val composeVersion: String by settings
    val kotlinxSerializationVersion: String by settings
    // Third-party
    val realmVersion: String by settings

    plugins {
        kotlin("multiplatform") version kotlinVersion apply false
        kotlin("plugin.serialization") version kotlinxSerializationVersion apply false
        id("org.jetbrains.compose") version composeVersion apply false
        id("io.realm.kotlin") version realmVersion apply false
    }

    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://jitpack.io")
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }

}
rootProject.name = "ArtCentro"


include(":android")
include(":desktop")
include(":common")
include(":compose-widgets")

