val targetSDKVersion: String by project.properties
val minSDKVersion: String by project.properties
val realmVersion: String by project.properties
val koinVersion: String by project.properties

plugins {
    id("org.jetbrains.compose")
    id("com.android.application")
    kotlin("android")
}

group = "me.aguragorn"
version = "1.0"

dependencies {
    implementation(project(":common"))
    implementation("androidx.activity:activity-compose:1.7.0")
    implementation("io.insert-koin:koin-android:$koinVersion")
    compileOnly("io.realm.kotlin:library-base:$realmVersion")
}

android {
    namespace = "me.aguragorn.artcentro.android"
    compileSdk = targetSDKVersion.toInt()
    defaultConfig {
        applicationId = "me.aguragorn.appcentro.android"
        minSdk = minSDKVersion.toInt()
        targetSdk = targetSDKVersion.toInt()
        versionCode = 1
        versionName = "1.0"
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
}

kotlin {
    jvmToolchain(11)
}