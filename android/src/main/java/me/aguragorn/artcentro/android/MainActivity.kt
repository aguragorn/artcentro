package me.aguragorn.artcentro.android

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import me.aguragorn.artcentro.common.app.ui.App
import me.aguragorn.artcentro.common.app.ui.AppView
import me.aguragorn.artcentro.common.auth.ui.AuthenticateView
import me.aguragorn.artcentro.common.timelines.ui.TimelineView
import me.aguragorn.artcentro.common.util.toURI
import me.aguragorn.artcentro.common.util.mainContext
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private val mainScope = CoroutineScope(mainContext)

    private val appView by inject<AppView>()
    private val authenticateView by inject<AuthenticateView>()
    private val timelineView by inject<TimelineView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MaterialTheme {
                App(
                    appView,
                    authenticateView,
                    timelineView,
                )
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        mainScope.launch {
            appView.appStart(intent.data?.toURI())
        }
    }
}
