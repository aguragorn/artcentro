// region VERSIONS
val kotlinxDateTimeVersion: String by project.properties
val kotlinxCoroutinesVersion: String by project.properties
val kotlinxSerializationJsonVersion: String by project.properties
val ktorVersion: String by project.properties

val targetSDKVersion: String by project.properties
val androidxAppCompatVersion: String by project.properties
val androidxBrowserVersion: String by project.properties
val androidxCoreVersion: String by project.properties

val pixelfed4kmppVersion: String by project.properties
val klogVersion: String by project.properties

val junitVersion: String by project.properties
val koinVersion: String by project.properties
val koinAnnotationsVersion: String by project.properties
val realmVersion: String by project.properties
val okhttpLoggingInterceptor: String by project.properties
// endregion

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("org.jetbrains.compose")
    id("com.android.library")
    id("io.realm.kotlin")
}

group = "me.aguragorn.artcentro"
version = "1.0"

kotlin {
    android()
    jvm("desktop") {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
    }
    sourceSets {
        getByName("commonMain") {
            dependencies {
                implementation(project(":compose-widgets"))

                // Compose
                api(compose.runtime)
                api(compose.foundation)
                api(compose.material)
                api(compose.ui)

                @OptIn(org.jetbrains.compose.ExperimentalComposeLibrary::class)
                implementation(compose.components.resources)

                // Jetbrains/Kotlin
                api("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationJsonVersion")

                // Internal Libraries
                api("com.gitlab.aguragorn.klog:KLog:$klogVersion")
                implementation("me.aguragorn.pixelfed4kmpp:lib:$pixelfed4kmppVersion")

                // Third-party Libraries
                api("io.insert-koin:koin-core:$koinVersion")
                implementation("com.squareup.okhttp3:logging-interceptor:$okhttpLoggingInterceptor")
                implementation("io.realm.kotlin:library-base:$realmVersion")
            }
        }
        getByName("commonTest") {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        getByName("androidMain") {
            dependencies {

                // Android
                api("androidx.appcompat:appcompat:$androidxAppCompatVersion")
                implementation("androidx.browser:browser:$androidxBrowserVersion")
                api("androidx.core:core-ktx:$androidxCoreVersion")

                // Third-party libraries
                api("io.insert-koin:koin-android:$koinVersion")
            }
        }
        getByName("androidTest") {
            dependencies {
                implementation("junit:junit:$junitVersion")
            }
        }
        getByName("desktopMain") {
            dependencies {
                // Compose
                api(compose.preview)

                // Jetbrains/Kotlin
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-swing:$kotlinxCoroutinesVersion")
            }
        }
        getByName("desktopTest")
    }
    jvmToolchain(11)
}

android {
    namespace = "me.aguragorn.artcentro.common"
    compileSdk = targetSDKVersion.toInt()
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 24
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    buildFeatures {
        buildConfig =true
    }
    buildTypes {
        getByName("release")
        getByName("debug")
    }
}