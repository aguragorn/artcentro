package me.aguragorn.artcentro.common.util

import org.junit.Test
import kotlin.test.assertEquals

private class Enclosing {
    companion object {}
}

private class EnclosingNamed {
    companion object Named {}
}

class ReflectionUtilsTest {
    @Test
    fun enclosingClassSimpleName_returns_Enclosing_on_Enclosing_Companion() {
        val clazz = Enclosing.Companion::class
        assertEquals("Enclosing", clazz.enclosingClassSimpleName)
    }

    @Test
    fun enclosingClassSimpleName_returns_EnclosingNamed_on_EnclosingName_Companion() {
        val clazz = EnclosingNamed.Named::class
        assertEquals("EnclosingNamed", clazz.enclosingClassSimpleName)
    }

    @Test
    fun enclosingClassName_returns_Enclosing_on_Enclosing_Companion() {
        val clazz = Enclosing.Companion::class
        assertEquals("me.aguragorn.artcentro.common.util.Enclosing", clazz.enclosingClassName)
    }

    @Test
    fun enclosingClassName_returns_EnclosingNamed_on_EnclosingName_Companion() {
        val clazz = EnclosingNamed.Named::class
        assertEquals("me.aguragorn.artcentro.common.util.EnclosingNamed", clazz.enclosingClassName)
    }
}