package me.aguragorn.artcentro.common.util.api

actual class NetworkValues {
    actual val artcentroOAuthScheme: String = "oauth2redirect"
    actual val artcentroOAuthHost: String = "me.aguragorn.artcentro"
    actual val artcentroOAuthUrl: String = "$artcentroOAuthScheme://$artcentroOAuthHost"
}
