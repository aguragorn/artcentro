package me.aguragorn.artcentro.common.app.store.mapper

import io.realm.kotlin.ext.toRealmList
import me.aguragorn.artcentro.common.app.domain.model.Prefs
import me.aguragorn.artcentro.common.app.store.model.PrefsEntity

fun Prefs.toPrefsEntity(): PrefsEntity = PrefsEntity().also { entity ->
    entity.id = id
    entity.currentInstance = currentInstance
    entity.accounts = accounts.map { it.toAccountEntity() }.toRealmList()
    entity.activeAccount = activeAccount?.toAccountEntity()
}

fun PrefsEntity.toPrefs(): Prefs = Prefs(
    id = id,
    currentInstance = currentInstance,
    accounts = accounts.map { it.toAccount() }.toMutableList(),
    activeAccount = activeAccount?.toAccount(),
)