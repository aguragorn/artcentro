package me.aguragorn.artcentro.common.util

import kotlin.reflect.KClass

val <T : Any> KClass<T>.enclosingClassName: String?
    get() = simpleName?.let { qualifiedName?.removeSuffix(".$it") }

val <T : Any> KClass<T>.enclosingClassSimpleName: String?
    get() = enclosingClassName?.substringAfterLast(".")
