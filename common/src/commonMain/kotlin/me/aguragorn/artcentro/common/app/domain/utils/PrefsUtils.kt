package me.aguragorn.artcentro.common.app.domain.utils

import me.aguragorn.artcentro.common.app.domain.model.Prefs

/**
 * Used to check if an account is in active use.
 */
fun Prefs.hasActiveAccount(): Boolean {
    return activeAccount != null
}