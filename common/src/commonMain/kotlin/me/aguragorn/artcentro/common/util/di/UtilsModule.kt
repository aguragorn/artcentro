package me.aguragorn.artcentro.common.util.di

import me.aguragorn.artcentro.common.auth.store.ArtCentroSecrets
import me.aguragorn.artcentro.common.util.api.PixelFedClientFactory
import me.aguragorn.artcentro.common.util.api.createPublicClient
import me.aguragorn.artcentro.common.util.api.createSecureClient
import me.aguragorn.artcentro.common.util.store.realm.RealmUtils
import org.koin.core.module.Module
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf

fun Module.utilsDi() {
    singleOf(::RealmUtils)
    factoryOf(::ArtCentroSecrets)
    factoryOf(::createPublicClient)
    factoryOf(::createSecureClient)
    singleOf(::PixelFedClientFactory)
}