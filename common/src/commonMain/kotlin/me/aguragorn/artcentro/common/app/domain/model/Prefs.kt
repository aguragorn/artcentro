package me.aguragorn.artcentro.common.app.domain.model

import java.util.*

data class Prefs(
    val id: String = "PREFS_ID",
    var currentInstance: String? = null,
    var accounts: MutableList<Account> = mutableListOf(),
    var activeAccount: Account? = null,
) {
    data class Account(
        val storeId: String = UUID.randomUUID().toString(),
        val username: String = "",
    )
}