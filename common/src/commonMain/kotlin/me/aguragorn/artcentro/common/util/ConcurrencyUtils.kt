package me.aguragorn.artcentro.common.util

import kotlinx.coroutines.Dispatchers
import me.aguragorn.artcentro.common.util.crashHandler

val ioContext = Dispatchers.IO + crashHandler
val mainContext = Dispatchers.Main + crashHandler
val defaultContext = Dispatchers.Default + crashHandler