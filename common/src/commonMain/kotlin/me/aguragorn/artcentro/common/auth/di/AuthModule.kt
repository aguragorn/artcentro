package me.aguragorn.artcentro.common.auth.di

import me.aguragorn.artcentro.common.auth.api.AuthApi
import me.aguragorn.artcentro.common.auth.domain.usecase.AuthenticateAccount
import me.aguragorn.artcentro.common.auth.domain.usecase.GetAuthorizationFrom
import me.aguragorn.artcentro.common.auth.store.SecretsStoreFactory
import me.aguragorn.artcentro.common.auth.ui.AuthenticateView
import org.koin.core.module.Module
import org.koin.core.module.dsl.singleOf

fun Module.authDi() {
    // Store
    singleOf(::SecretsStoreFactory)
    // API
    singleOf(::AuthApi)
    // Domain
    singleOf(::GetAuthorizationFrom)
    singleOf(::AuthenticateAccount)
    // UI
    singleOf(::AuthenticateView)
}
