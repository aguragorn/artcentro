package me.aguragorn.artcentro.common.auth.store.model

import io.realm.kotlin.types.RealmObject

class TokenEntity : RealmObject {
    var accessToken: String? = null
    var refreshToken: String? = null
    var tokenType: String? = null
    var scope: String? = null
    var createdAt: Int? = null
}