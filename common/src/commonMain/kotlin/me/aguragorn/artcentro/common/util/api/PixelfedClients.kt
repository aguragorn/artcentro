package me.aguragorn.artcentro.common.util.api

import me.aguragorn.pixelfed4kmpp.lib.PixelfedPublicClient
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.authentication.store.SecretsStore

expect fun createPublicClient(
    instanceName: String,
    secretsStore: SecretsStore,
): PixelfedPublicClient

expect fun createSecureClient(
    instanceName: String,
    secretsStore: SecretsStore,
): PixelfedSecureClient