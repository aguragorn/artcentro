package me.aguragorn.artcentro.common.util.service

import java.net.URI

interface DeviceService {
    fun openBrowserTo(uri: URI)
}