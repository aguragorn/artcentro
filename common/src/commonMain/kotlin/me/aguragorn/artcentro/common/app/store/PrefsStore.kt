package me.aguragorn.artcentro.common.app.store

import io.realm.kotlin.UpdatePolicy
import io.realm.kotlin.ext.query
import kotlinx.coroutines.withContext
import me.aguragorn.artcentro.common.app.domain.model.Prefs
import me.aguragorn.artcentro.common.app.store.mapper.toPrefs
import me.aguragorn.artcentro.common.app.store.mapper.toPrefsEntity
import me.aguragorn.artcentro.common.app.store.model.PrefsEntity
import me.aguragorn.artcentro.common.util.store.realm.RealmUtils
import me.aguragorn.artcentro.common.util.ioContext

class PrefsStore(
    private val realmUtils: RealmUtils
) {
    suspend fun get(): Prefs? = withContext(ioContext) {
        realmUtils.getDefaultStore()
            .query<PrefsEntity>()
            .first().find()
            ?.toPrefs()
    }

    suspend fun save(prefs: Prefs) = withContext(ioContext) {
        realmUtils.getDefaultStore().writeBlocking {
            copyToRealm(prefs.toPrefsEntity(), updatePolicy = UpdatePolicy.ALL)
        }
    }
}