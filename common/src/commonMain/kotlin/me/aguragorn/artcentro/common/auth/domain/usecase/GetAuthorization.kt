package me.aguragorn.artcentro.common.auth.domain.usecase

import kotlinx.coroutines.withContext
import me.aguragorn.artcentro.common.auth.api.AuthApi
import me.aguragorn.artcentro.common.auth.domain.model.APP_SCOPE
import me.aguragorn.artcentro.common.auth.store.SecretsStoreFactory
import me.aguragorn.artcentro.common.util.api.NetworkValues
import me.aguragorn.artcentro.common.util.ioContext
import java.net.URI

class GetAuthorizationFrom(
    private val authApi: AuthApi,
    private val networkValues: NetworkValues,
    private val secretsStoreFactory: SecretsStoreFactory,
) {

    /**
     * Generates an Authorization URI for the [instance].
     */
    suspend operator fun invoke(
        instance: String
    ): URI = withContext(ioContext) {

        val secretsStore = secretsStoreFactory.getFor(instance)

        val secrets = authApi.registerApp(
            clientName = "Art Centro",
            redirectUris = networkValues.artcentroOAuthUrl,
            scope = APP_SCOPE,
        )
        secretsStore.saveSecrets(secrets)

        // TODO: use info from nodeInfo and instance
//        val nodeInfoJRD = publicClient.getWellKnownNodeInfo()
//        val nodeInfoSchemaUrl = nodeInfoJRD.links.firstOrNull {
//            it.rel == "http://nodeinfo.diaspora.software/ns/schema/2.0"
//        }?.href ?: throw AuthorizationException("Node info schema url unavailable.")
//
//        val nodeInfo = publicClient.getNodeInfo(nodeInfoSchemaUrl)
//        val domain = publicClient.getInstance().uri

        return@withContext authApi.getAuthorizationUrl(
            clientId = secrets.clientId,
            redirectUri = networkValues.artcentroOAuthUrl,
        )
    }
}