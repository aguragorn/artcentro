package me.aguragorn.artcentro.common.util

import kotlinx.coroutines.flow.MutableStateFlow
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T> MutableStateFlow<T>.delegate(): ReadWriteProperty<Any, T> {
    return object : ReadWriteProperty<Any, T> {
        override fun getValue(thisRef: Any, property: KProperty<*>): T {
            return this@delegate.value
        }

        override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
            this@delegate.value = value
        }
    }
}