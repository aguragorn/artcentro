package me.aguragorn.artcentro.common.app.di

import me.aguragorn.artcentro.common.app.store.PrefsStore
import me.aguragorn.artcentro.common.app.ui.AppView
import me.aguragorn.artcentro.common.app.domain.usecase.SetCurrentInstance
import me.aguragorn.artcentro.common.auth.domain.util.OAuthValidator
import org.koin.core.module.Module
import org.koin.core.module.dsl.singleOf

fun Module.appDi() {
    // Utils
    singleOf(::OAuthValidator)
    // Store
    singleOf(::PrefsStore)
    // Domain
    singleOf(::SetCurrentInstance)
    // UI
    singleOf(::AppView)
}