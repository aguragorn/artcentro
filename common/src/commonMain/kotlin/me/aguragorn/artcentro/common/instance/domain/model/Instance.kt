package me.aguragorn.artcentro.common.instance.domain.model

data class Instance (
    val description: String?,
    val email: String?,
    val max_toot_chars: Int?,
    val registrations: Boolean?,
    val thumbnail: String?,
    val title: String?,
    val uri: String?,
    val version: String?
)