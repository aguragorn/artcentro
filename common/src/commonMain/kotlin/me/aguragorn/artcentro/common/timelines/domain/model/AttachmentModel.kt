package me.aguragorn.artcentro.common.timelines.domain.model

import io.ktor.http.*

data class AttachmentModel(
    val remoteId: String?,
    val type: Type = Type.unknown,
    val preview: Url?,
    val original: Url?,
    val description: String?,
    val blurHash: String?,
) {
    enum class Type {
        unknown, image, gifv, video, audio
    }
}
