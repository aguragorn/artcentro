package me.aguragorn.artcentro.common.auth.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Token(
    val accessToken: String? = null,
    val refreshToken: String? = null,
    val tokenType: String? = null,
    val scope: String? = null,
    val createdAt: Int? = null,
)