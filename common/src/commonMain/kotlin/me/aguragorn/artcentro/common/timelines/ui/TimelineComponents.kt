package me.aguragorn.artcentro.common.timelines.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import me.aguragorn.artcentro.common.timelines.domain.model.AttachmentModel
import me.aguragorn.artcentro.common.timelines.domain.model.AuthorModel
import me.aguragorn.artcentro.common.timelines.domain.model.PostModel
import me.aguragorn.artcentro.common.util.ioContext
import me.aguragorn.artcentro.common.util.mainContext
import me.aguragorn.compose.widgets.image.utils.ImageLoader
import me.aguragorn.compose.widgets.text.HtmlText
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource

@OptIn(ExperimentalResourceApi::class)
@Composable
fun Timeline(
    timelineView: TimelineView,
) {
    val listState = rememberLazyListState()
    val statuses by timelineView.statusesState.collectAsState()

    LaunchedEffect(statuses) {
        launch(mainContext) { timelineView.loadTimeline() }
    }

    Column {
        LazyColumn(state = listState) {
            items(statuses) { status ->
                Post(status)
            }
        }
        Row {
            Image(painterResource("ic_home_timeline.svg"), null)
        }
    }

}

@Composable
fun Post(
    postModel: PostModel
) {
    Column(
        modifier = Modifier.padding(
            top = 8.dp,
            bottom = 8.dp,
        )
    ) {
        Author(postModel.author)

        Media(postModel.attachments)

        Caption(postModel.caption)
    }
}

@Composable
private fun Media(attachments: List<AttachmentModel>) {
    attachments.firstOrNull()?.original?.let { url ->
        val menuImage = ImageLoader.getBitmapFrom(url.toString())
            .flowOn(ioContext)
            .collectAsState(null).value

        if (menuImage != null) {
            Image(
                menuImage,
                contentDescription = "",
                modifier = Modifier.fillMaxWidth(),
            )
        }
    }
}

@Composable
private fun Author(author: AuthorModel) {
    Text(
        text = author.displayName,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.padding(horizontal = 4.dp),
    )
}

@Composable
private fun Caption(caption: String) {
    HtmlText(
        content = caption,
        modifier = Modifier.padding(horizontal = 4.dp),
    )
}