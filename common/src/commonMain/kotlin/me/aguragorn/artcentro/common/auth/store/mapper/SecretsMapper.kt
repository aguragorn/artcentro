package me.aguragorn.artcentro.common.auth.store.mapper

import me.aguragorn.artcentro.common.auth.domain.model.Secrets
import me.aguragorn.artcentro.common.auth.store.model.SecretsEntity
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Secrets as SecretsDto

fun SecretsDto(): SecretsDto = SecretsDto(
    clientId = "",
    clientSecret = "",
    lastToken = TokenDto()
)

fun SecretsEntity.toSecretsDto(): SecretsDto {
    return SecretsDto(
        clientId = clientId,
        clientSecret = clientSecret,
        lastToken = lastToken?.toTokenDto() ?: TokenDto()
    )
}

fun SecretsDto.toSecretsEntity(): SecretsEntity {
    return SecretsEntity().also { entity ->
        entity.clientId = clientId
        entity.clientSecret = clientSecret
        entity.lastToken = lastToken.toTokenEntity()
    }
}

fun SecretsEntity.toSecrets(): Secrets {
    return Secrets(
        clientId = clientId,
        clientSecret = clientSecret,
        lastToken = lastToken?.toToken()
    )
}

fun Secrets.toSecretsEntity(): SecretsEntity {
    return SecretsEntity().also { entity ->
        entity.clientId = clientId
        entity.clientSecret = clientSecret
        entity.lastToken = lastToken?.toTokenEntity()
    }
}