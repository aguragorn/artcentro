package me.aguragorn.artcentro.common.app.store.mapper

import me.aguragorn.artcentro.common.app.domain.model.Prefs
import me.aguragorn.artcentro.common.app.store.model.PrefsAccountEntity

fun Prefs.Account.toAccountEntity(): PrefsAccountEntity = PrefsAccountEntity().also {
    it.storeId = storeId
    it.username = username
}

fun PrefsAccountEntity.toAccount(): Prefs.Account = Prefs.Account(
    storeId = storeId,
    username = username
)