package me.aguragorn.artcentro.common.app.domain.usecase

import me.aguragorn.artcentro.common.app.domain.model.Prefs
import me.aguragorn.artcentro.common.app.store.PrefsStore

class SetCurrentInstance(
    private val prefsStore: PrefsStore,
) {
    suspend operator fun invoke(
        instance: String
    ) {
        val prefs = prefsStore.get() ?: Prefs()
        prefs.currentInstance = instance
        prefsStore.save(prefs)
    }
}