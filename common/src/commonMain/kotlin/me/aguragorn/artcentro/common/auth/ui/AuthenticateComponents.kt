package me.aguragorn.artcentro.common.auth.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import kotlinx.coroutines.launch
import me.aguragorn.artcentro.common.util.mainContext
import me.aguragorn.compose.widgets.text.OutlinedTextField

@Composable
fun InstanceEntry(viewModel: AuthenticateView) {
    val composableScope = rememberCoroutineScope()

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // TODO: message saying connect to $instanceState.value
        // TODO: add link to change instance (displays Domain Entry Text Field)
        OutlinedTextField(
            valueState = viewModel.instanceState,
            label = { Text("Domain of your Instance") },
        )
        Button(onClick = {
            composableScope.launch(mainContext) {
                viewModel.authenticate()
            }
        }) {
            Text("Connect to Instance")
        }
    }

}