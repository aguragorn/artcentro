package me.aguragorn.artcentro.common.timelines.ui

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import me.aguragorn.artcentro.common.timelines.domain.model.PostModel
import me.aguragorn.artcentro.common.timelines.domain.model.Timeline
import me.aguragorn.artcentro.common.timelines.domain.usecase.GetTimelineStatuses
import me.aguragorn.artcentro.common.util.Response
import me.aguragorn.artcentro.common.util.delegate
import me.aguragorn.artcentro.common.util.logger
import me.aguragorn.artcentro.common.util.mainContext

class TimelineView(
    private val getTimelineStatuses: GetTimelineStatuses,
) {
    companion object {
        private val log by logger()
    }

    val timelineState: MutableStateFlow<Timeline> = MutableStateFlow(Timeline.Home)
    private var timeline by timelineState.delegate()

    val statusesState: MutableStateFlow<List<PostModel>> = MutableStateFlow(emptyList())
    private var statuses by statusesState.delegate()

    init {
        CoroutineScope(mainContext).launch {
            statusesState.collectLatest(::debugStatuses)
        }
    }

    suspend fun loadTimeline() {
        log.i { "Loading ${timeline.title} timeline" }

        statuses = when (val response = getTimelineStatuses(timeline)) {
            is Response.Success -> response.content
            is Response.Failure -> {
                log.e(response.error)
                emptyList()
            }
        }
    }

    private fun debugStatuses(statuses: List<PostModel>) {

    }
}