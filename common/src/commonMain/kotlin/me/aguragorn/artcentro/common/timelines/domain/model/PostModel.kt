package me.aguragorn.artcentro.common.timelines.domain.model

import kotlinx.datetime.Instant

data class PostModel(
    val remoteId: String,
    val createdAt: Instant?,
    val author: AuthorModel,
    val sensitive: Boolean,
    val spoilerText: String?,
    val attachments: List<AttachmentModel>,
    val caption: String,
    val shareCount: Long,
    val isSharedByMe: Boolean?,
    val favoritesCount: Long,
    val isFavoriteByMe: Boolean?,
    val repliesCount: Long,
    val repostOf: PostModel?,
    val isMutedByMe: Boolean?,
    val isPinnedByMe: Boolean?,
    val isBookmarkedByMe: Boolean?,
)