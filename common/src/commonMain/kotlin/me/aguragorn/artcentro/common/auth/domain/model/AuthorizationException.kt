package me.aguragorn.artcentro.common.auth.domain.model

class AuthorizationException(
    message: String,
    cause: Throwable? = null
): Exception(
    message, cause
)