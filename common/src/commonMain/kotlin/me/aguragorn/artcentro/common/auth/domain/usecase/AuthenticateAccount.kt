package me.aguragorn.artcentro.common.auth.domain.usecase

import me.aguragorn.artcentro.common.app.domain.model.Prefs
import me.aguragorn.artcentro.common.app.store.PrefsStore
import me.aguragorn.artcentro.common.auth.api.AuthApi
import me.aguragorn.artcentro.common.auth.domain.model.ACCOUNT_SCOPE
import me.aguragorn.artcentro.common.auth.store.SecretsStoreFactory
import me.aguragorn.artcentro.common.util.api.NetworkValues
import java.net.URI

class AuthenticateAccount(
    private val authApi: AuthApi,
    private val prefsStore: PrefsStore,
    private val secretsStoreFactory: SecretsStoreFactory,
    private val networkValues: NetworkValues
) {
    suspend operator fun invoke(oAuthRedirectUri: URI): Boolean {
        val currentInstance = prefsStore.get()?.currentInstance ?: run {
            // TODO: Show error screen or show auth screen again or throw
            return false
        }

        val appSecretsStore = secretsStoreFactory.getFor(currentInstance)
        val secrets = appSecretsStore.getSecrets()

        val code = authApi.extractAuthorizationCode(oAuthRedirectUri)
        val token = authApi.authenticateAccount(
            secrets = secrets,
            code = code,
            redirectUri = networkValues.artcentroOAuthUrl,
            scope = ACCOUNT_SCOPE
        )

        // new account has been authenticated
        val acct = Prefs.Account()

        // update accounts for app restart
        prefsStore.get()?.let { prefs ->
            prefs.accounts.add(acct)
            prefs.activeAccount = acct
            prefsStore.save(prefs)
        }

        val memberSecretsStore = secretsStoreFactory.getFor(acct.storeId)
        memberSecretsStore.saveSecrets(secrets.copy(lastToken = token))

        return true
    }
}