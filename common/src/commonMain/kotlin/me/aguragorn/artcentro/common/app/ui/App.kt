package me.aguragorn.artcentro.common.app.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import me.aguragorn.artcentro.common.auth.ui.AuthenticateView
import me.aguragorn.artcentro.common.auth.ui.InstanceEntry
import me.aguragorn.artcentro.common.timelines.ui.Timeline
import me.aguragorn.artcentro.common.timelines.ui.TimelineView

@Composable
fun App(
    appView: AppView,
    authenticateView: AuthenticateView,
    timelineView: TimelineView,
) {
    val screen by appView.currentScreenState.collectAsState()

    when (screen) {
        is AppView.Screen.TimelineScreen -> Timeline(
            timelineView = timelineView
        )
        is AppView.Screen.InstanceSelectScreen -> InstanceEntry(
            viewModel = authenticateView
        )
        else -> { /* TODO: Splash Screen */
        }
    }
}
