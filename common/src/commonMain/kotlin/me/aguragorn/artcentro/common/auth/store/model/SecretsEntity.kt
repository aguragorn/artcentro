package me.aguragorn.artcentro.common.auth.store.model

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

class SecretsEntity : RealmObject {
    @PrimaryKey
    var clientId: String = ""
    var clientSecret: String = ""
    var lastToken: TokenEntity? = null
}