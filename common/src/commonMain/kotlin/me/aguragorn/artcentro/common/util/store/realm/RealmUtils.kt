package me.aguragorn.artcentro.common.util.store.realm

import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import me.aguragorn.artcentro.common.app.store.model.PrefsAccountEntity
import me.aguragorn.artcentro.common.app.store.model.PrefsEntity
import me.aguragorn.artcentro.common.auth.store.model.SecretsEntity
import me.aguragorn.artcentro.common.auth.store.model.TokenEntity
import me.aguragorn.artcentro.common.util.store.StoreValues

const val DEFAULT_DB = "default"
const val DB_DIR = "data/db"


class RealmUtils(
    private val storeValues: StoreValues
) {
    fun getSecretsStoreOf(dbName: String): Realm {
        val config = RealmConfiguration
            .Builder(
                setOf(
                    SecretsEntity::class,
                    TokenEntity::class
                )
            )
            .directory(storeValues.dbDir)
            .name("$dbName.realm")
            .build()

        return Realm.open(config)
    }

    fun getDefaultStore(): Realm {
        val config = RealmConfiguration
            .Builder(
                setOf(
                    PrefsEntity::class,
                    PrefsAccountEntity::class
                )
            )
            .directory(storeValues.dbDir)
            .name("${storeValues.defaultDB}.realm")
            .build()

        return Realm.open(config)
    }
}
