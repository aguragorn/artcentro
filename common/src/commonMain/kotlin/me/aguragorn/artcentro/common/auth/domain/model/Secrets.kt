package me.aguragorn.artcentro.common.auth.domain.model

data class Secrets(
    val clientId: String = "",
    val clientSecret: String = "",
    val lastToken: Token? = null,
)