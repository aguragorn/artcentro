package me.aguragorn.artcentro.common.app.store.model

import io.realm.kotlin.types.RealmObject

class PrefsAccountEntity : RealmObject {
    var storeId: String = ""
    var username: String = ""
}