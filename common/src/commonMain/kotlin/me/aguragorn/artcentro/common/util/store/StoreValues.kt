package me.aguragorn.artcentro.common.util.store

expect class StoreValues {
    val dbDir: String
    val defaultDB: String
}