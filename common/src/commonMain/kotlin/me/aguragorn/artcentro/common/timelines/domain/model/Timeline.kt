package me.aguragorn.artcentro.common.timelines.domain.model

sealed interface Timeline {
    val title: String

    /**
     * Home timeline of the logged-in member.
     */
    object Home : Timeline {
        override val title: String = "Home"
    }

    /**
     * Local timeline of the instance.
     */
    class Local(name: String) : Timeline {
        override val title: String = name
    }

    /**
     * Federated timeline. Public timeline of other instances that the current instance knows about.
     */
    object Global : Timeline {
        override val title: String = "Global"
    }

    /**
     * Timeline for a specified tag.
     */
    class Tag(val name: String) : Timeline {
        override val title: String = name
    }

    /**
     * Timeline for a specified list.
     */
    class List(val id: String, name: String) : Timeline {
        override val title: String = name
    }
}