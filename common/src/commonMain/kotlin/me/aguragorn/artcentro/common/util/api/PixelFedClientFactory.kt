package me.aguragorn.artcentro.common.util.api

import me.aguragorn.artcentro.common.app.store.PrefsStore
import me.aguragorn.artcentro.common.auth.store.ArtCentroSecrets
import me.aguragorn.artcentro.common.util.logger
import me.aguragorn.artcentro.common.util.store.realm.RealmUtils
import me.aguragorn.pixelfed4kmpp.lib.PixelfedPublicClient
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.parameter.parametersOf


class PixelFedClientFactory(
    private val prefsStore: PrefsStore,
    private val realmUtils: RealmUtils,
) : KoinComponent {
    private val log by logger()

    suspend fun getPublicClient(): PixelfedPublicClient {
        val instanceName = prefsStore.get()?.currentInstance ?: ""
        val secretsStore: ArtCentroSecrets by inject { parametersOf(instanceName, realmUtils) }
        return createPublicClient(instanceName, secretsStore)
    }

    suspend fun getSecureClient(): PixelfedSecureClient {
        val instanceName = prefsStore.get()?.currentInstance ?: ""
        val storeId = prefsStore.get()?.activeAccount?.storeId ?: ""
        val secretsStore: ArtCentroSecrets by inject { parametersOf(storeId, realmUtils) }
        return createSecureClient(instanceName, secretsStore)
    }
}