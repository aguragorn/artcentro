package me.aguragorn.artcentro.common.app.ui

import kotlinx.coroutines.flow.MutableStateFlow
import me.aguragorn.artcentro.common.app.domain.utils.hasActiveAccount
import me.aguragorn.artcentro.common.app.store.PrefsStore
import me.aguragorn.artcentro.common.auth.domain.usecase.AuthenticateAccount
import me.aguragorn.artcentro.common.auth.domain.util.OAuthValidator
import me.aguragorn.artcentro.common.util.delegate
import me.aguragorn.artcentro.common.util.logger
import java.net.URI

class AppView(
    // Dependencies
    private val oAuthValidator: OAuthValidator,
    private val prefsStore: PrefsStore,
    // UseCases
    private val authenticateAccount: AuthenticateAccount,
) {

    val currentScreenState: MutableStateFlow<Screen> = MutableStateFlow(Screen.SplashScreen)
    private var currentScreen by currentScreenState.delegate()

    suspend fun appStart(redirectUri: URI?) {
        log.i { "App Starting" }
        val prefs = prefsStore.get()
        when {
            redirectUri != null -> handleRedirectUri(redirectUri)
            prefs?.hasActiveAccount() == true -> showTimeLine()
            else -> showDomainEntry()
        }
    }

    private fun showDomainEntry() {
        log.i { "Show Instance Select Screen" }
        currentScreen = Screen.InstanceSelectScreen
    }

    private fun showTimeLine() {
        log.i { "Show Timeline Screen" }
        currentScreen = Screen.TimelineScreen
    }

    private suspend fun handleRedirectUri(redirectUri: URI) {
        // restart authentication flow if invalid redirect uri is received
        // ArtCentro does not handle unknown URIs
        if (!oAuthValidator.isArtCentroOAuthRedirect(redirectUri)) {
            // show error
            showDomainEntry()
            return
        }

        if (authenticateAccount(redirectUri)) {
            showTimeLine()
        } else {
            showDomainEntry()
        }
    }

    companion object {
        private val log by logger()
    }

    sealed interface Screen {
        object SplashScreen : Screen
        object InstanceSelectScreen : Screen
        object TimelineScreen : Screen
    }
}