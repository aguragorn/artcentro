package me.aguragorn.artcentro.common.util

import com.aguragorn.klog.Logger
import kotlinx.coroutines.CoroutineExceptionHandler

fun <R : Any> R.logger(): Lazy<Logger> {
    val loggerName = if (this::class.isCompanion) {
        this::class.enclosingClassSimpleName
    } else {
        this::class.simpleName
    }
    return loggerDelegate(loggerName)
}

fun loggerWithName(name: String): Lazy<Logger> {
    return loggerDelegate(name)
}

private fun loggerDelegate(loggerName: String?) = lazy { Logger.withName("ac_$loggerName") }


private val unhandledExceptionLogger by loggerWithName("LoggingUtils")

/**
 * Always use this when launching coroutines especially if we are not waiting for the job to finish
 */
val crashHandler = CoroutineExceptionHandler { _, throwable ->
    unhandledExceptionLogger.e(throwable) { "Uncaught exception encountered" }
}
