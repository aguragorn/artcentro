package me.aguragorn.artcentro.common.auth.store

import io.realm.kotlin.UpdatePolicy
import io.realm.kotlin.ext.query
import kotlinx.coroutines.withContext
import me.aguragorn.artcentro.common.auth.domain.model.Secrets
import me.aguragorn.artcentro.common.auth.store.mapper.SecretsDto
import me.aguragorn.artcentro.common.auth.store.mapper.toSecrets
import me.aguragorn.artcentro.common.auth.store.mapper.toSecretsDto
import me.aguragorn.artcentro.common.auth.store.mapper.toSecretsEntity
import me.aguragorn.artcentro.common.auth.store.model.SecretsEntity
import me.aguragorn.artcentro.common.util.store.realm.RealmUtils
import me.aguragorn.artcentro.common.util.ioContext
import me.aguragorn.pixelfed4kmpp.lib.authentication.store.SecretsStore
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Secrets as SecretsDto

class ArtCentroSecrets(
    private val dbName: String,
    private val realmUtils: RealmUtils,
) : SecretsStore {

    suspend fun getSecrets(): Secrets = withContext(ioContext) {
        return@withContext realmUtils.getSecretsStoreOf(dbName)
            .query<SecretsEntity>()
            .first().find()
            ?.toSecrets() ?: Secrets()
    }

    suspend fun saveSecrets(
        secrets: Secrets
    ) = withContext<Unit>(ioContext) {
        realmUtils.getSecretsStoreOf(dbName).writeBlocking {
            val entity = secrets.toSecretsEntity()
            copyToRealm(entity, UpdatePolicy.ALL)
        }
    }

    override suspend fun get(): SecretsDto = withContext(ioContext) {
        return@withContext realmUtils.getSecretsStoreOf(dbName)
            .query<SecretsEntity>()
            .first().find()
            ?.toSecretsDto() ?: SecretsDto()
    }

    override suspend fun save(
        secrets: SecretsDto
    ) = withContext<Unit>(ioContext) {
        realmUtils.getSecretsStoreOf(dbName).writeBlocking {
            val entity = secrets.toSecretsEntity()
            copyToRealm(entity, UpdatePolicy.ALL)
        }
    }
}