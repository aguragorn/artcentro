package me.aguragorn.artcentro.common.timelines.di

import me.aguragorn.artcentro.common.timelines.api.TimelineApi
import me.aguragorn.artcentro.common.timelines.domain.usecase.GetTimelineStatuses
import me.aguragorn.artcentro.common.timelines.ui.TimelineView
import org.koin.core.module.Module
import org.koin.core.module.dsl.singleOf


fun Module.timelineDi() {
    // Api
    singleOf(::TimelineApi)
    // Domain
    singleOf(::GetTimelineStatuses)
    // UI
    singleOf(::TimelineView)
}
