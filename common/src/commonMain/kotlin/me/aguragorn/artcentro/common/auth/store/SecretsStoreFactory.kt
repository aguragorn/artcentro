package me.aguragorn.artcentro.common.auth.store

import me.aguragorn.artcentro.common.util.store.realm.RealmUtils

class SecretsStoreFactory(
    private val realmUtils: RealmUtils,
) {
    private val stores = mutableMapOf<String, ArtCentroSecrets>()

    fun getFor(storeId: String): ArtCentroSecrets {
        return stores[storeId] ?: ArtCentroSecrets(storeId, realmUtils)
            .also { stores[storeId] = it }
    }
}