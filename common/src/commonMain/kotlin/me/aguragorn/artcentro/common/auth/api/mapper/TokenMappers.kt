package me.aguragorn.artcentro.common.auth.api.mapper

import me.aguragorn.artcentro.common.auth.domain.model.Token
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Token as TokenDto

fun TokenDto.toToken(): Token {
    return Token(
        accessToken = access_token,
        refreshToken = refresh_token,
        tokenType = token_type,
        scope = scope,
        createdAt = created_at,
    )
}