package me.aguragorn.artcentro.common.timelines.api

import io.ktor.http.*
import me.aguragorn.artcentro.common.timelines.domain.model.AttachmentModel
import me.aguragorn.artcentro.common.timelines.domain.model.AuthorModel
import me.aguragorn.artcentro.common.timelines.domain.model.PostModel
import me.aguragorn.artcentro.common.util.unknownAccountUsername
import me.aguragorn.pixelfed4kmpp.lib.account.model.Account
import me.aguragorn.pixelfed4kmpp.lib.content.model.Attachment
import me.aguragorn.pixelfed4kmpp.lib.content.model.Status

fun Account.toAuthor(): AuthorModel = AuthorModel(
    displayName = display_name ?: unknownAccountUsername,
    acctUrl = Url(acct),
    staticAvatar = avatar_static?.let { Url(it) }
)

fun Attachment.toAttachmentModel(): AttachmentModel = AttachmentModel(
    remoteId = id,
    type = type?.name?.let { AttachmentModel.Type.valueOf(it) } ?: AttachmentModel.Type.unknown,
    preview = preview_url?.let { Url(it) },
    original = url?.let { Url(it) },
    description = description,
    blurHash = blurhash,
)

fun Status.toPostModel(): PostModel = PostModel(
    remoteId = id,
    createdAt = created_at,
    author = account?.toAuthor() ?: AuthorModel.unknown,
    sensitive = sensitive == true,
    spoilerText = spoiler_text,
    attachments = media_attachments?.map { it.toAttachmentModel() }.orEmpty(),
    caption = content.orEmpty(),
    shareCount = reblogs_count?.toLong() ?: 0L,
    isSharedByMe = reblogged,
    favoritesCount = favourites_count?.toLong() ?: 0L,
    isFavoriteByMe = favourited,
    repliesCount = replies_count?.toLong() ?: 0L,
    repostOf = reblog?.toPostModel(),
    isMutedByMe = muted,
    isPinnedByMe = pinned,
    isBookmarkedByMe = bookmarked,
)