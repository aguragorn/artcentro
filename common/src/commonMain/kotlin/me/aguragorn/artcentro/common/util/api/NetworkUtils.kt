package me.aguragorn.artcentro.common.util.api

import java.net.URI
import java.net.URLDecoder

expect class NetworkValues {
    val artcentroOAuthUrl: String
    val artcentroOAuthScheme: String
    val artcentroOAuthHost: String
}

val URI.queryParams: Map<String, List<String>>
    get() {
        val params = mutableMapOf<String, MutableList<String>>()

        for (param in rawQuery.split("&")) {
            val pair = param.split("=")

            if (pair.size != 2) continue

            val key = pair[0].decoded()
            val value = pair[1].decoded()

            val values = params[key] ?: mutableListOf<String>().also { params[key] = it }
            values.add(value)
        }

        return params
    }

private fun String.decoded() = URLDecoder.decode(this, "UTF-8")