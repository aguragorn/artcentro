package me.aguragorn.artcentro.common.util

sealed interface Response<out T> {
    class Success<T>(
        val content: T
    ) : Response<T>

    class Failure(
        val error: Exception
    ) : Response<Nothing>
}