package me.aguragorn.artcentro.common.timelines.domain.usecase

import me.aguragorn.artcentro.common.timelines.api.TimelineApi
import me.aguragorn.artcentro.common.timelines.domain.model.PostModel
import me.aguragorn.artcentro.common.timelines.domain.model.Timeline
import me.aguragorn.artcentro.common.timelines.domain.model.UnsupportedTimelineError
import me.aguragorn.artcentro.common.util.Response

class GetTimelineStatuses(
    private val timelineApi: TimelineApi
) {
    suspend operator fun invoke(timeline: Timeline): Response<List<PostModel>> {
        return when (timeline) {
            is Timeline.Home -> timelineApi.getHomeTimelineStatuses()
            else -> Response.Failure(error = UnsupportedTimelineError(timeline))
        }
    }
}