package me.aguragorn.artcentro.common.timelines.domain.model

class UnsupportedTimelineError(timeline: Timeline) : Exception(
    "${timeline::class.simpleName} is not yet supported."
)