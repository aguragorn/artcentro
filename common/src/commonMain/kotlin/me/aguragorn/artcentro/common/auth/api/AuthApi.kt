package me.aguragorn.artcentro.common.auth.api

import kotlinx.coroutines.withContext
import me.aguragorn.artcentro.common.auth.api.mapper.toToken
import me.aguragorn.artcentro.common.auth.domain.model.*
import me.aguragorn.artcentro.common.util.api.PixelFedClientFactory
import me.aguragorn.artcentro.common.util.ioContext
import me.aguragorn.pixelfed4kmpp.lib.application.api.registerApp
import me.aguragorn.pixelfed4kmpp.lib.application.model.RegisterAppParams
import me.aguragorn.pixelfed4kmpp.lib.authentication.api.authenticateAccount
import me.aguragorn.pixelfed4kmpp.lib.authentication.api.extractAuthorizationCode
import me.aguragorn.pixelfed4kmpp.lib.authentication.api.getAuthorizationUrl
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.AuthenticateAccountParams
import java.net.URI

class AuthApi(
    private val pixelFedClientFactory: PixelFedClientFactory
) {
    private suspend fun publicClient() = pixelFedClientFactory.getPublicClient()

    suspend fun registerApp(
        clientName: String,
        redirectUris: String,
        scope: String = APP_SCOPE,
        website: String? = null,
    ): Secrets = withContext(ioContext) {
        val app = publicClient().registerApp(
            RegisterAppParams(
                client_name = clientName,
                redirect_uris = redirectUris,
                scopes = scope,
                website = website
            )
        )

        val clientId = app.client_id ?: throw AuthorizationException("Client ID unavailable.")

        return@withContext Secrets(
            clientId = clientId,
            clientSecret = app.client_secret.orEmpty(),
        )
    }

    suspend fun getAuthorizationUrl(
        clientId: String,
        redirectUri: String,
    ): URI {
        return publicClient().getAuthorizationUrl(
            clientId = clientId,
            redirectUri = redirectUri,
            scopes = ACCOUNT_SCOPE,
        )
    }

    suspend fun extractAuthorizationCode(
        oAuthRedirectUri: URI
    ): String {
        return publicClient().extractAuthorizationCode(
            oauthRedirectUri = oAuthRedirectUri.toString()
        )
    }

    suspend fun authenticateAccount(
        secrets: Secrets,
        code: String,
        redirectUri: String,
        scope: String,
    ): Token {
        return publicClient().authenticateAccount(
            AuthenticateAccountParams(
                client_id = secrets.clientId,
                client_secret = secrets.clientSecret,
                redirect_uri = redirectUri,
                code = code,
                scope = scope,
            )
        ).toToken()
    }
}