package me.aguragorn.artcentro.common.auth.ui

import kotlinx.coroutines.flow.MutableStateFlow
import me.aguragorn.artcentro.common.app.domain.usecase.SetCurrentInstance
import me.aguragorn.artcentro.common.auth.domain.usecase.GetAuthorizationFrom
import me.aguragorn.artcentro.common.util.delegate
import me.aguragorn.artcentro.common.util.logger
import me.aguragorn.artcentro.common.util.service.DeviceService

class AuthenticateView(
    private val setCurrentInstance: SetCurrentInstance,
    private val getAuthorizationFrom: GetAuthorizationFrom,
    private var deviceService: DeviceService,
) {
    val instanceState = MutableStateFlow("pixelfed.social")
    private val instance by instanceState.delegate()

    suspend fun authenticate() {
        setCurrentInstance(instance)
        val authorizationUrl = getAuthorizationFrom(instance = instance)
        log.d { "launching browser with url: $authorizationUrl" }
        deviceService.openBrowserTo(authorizationUrl)
    }

    companion object {
        private val log by logger()
    }
}