package me.aguragorn.artcentro.common.auth.store.mapper

import me.aguragorn.artcentro.common.auth.domain.model.Token
import me.aguragorn.artcentro.common.auth.store.model.TokenEntity
import me.aguragorn.pixelfed4kmpp.lib.authentication.model.Token as TokenDto


fun TokenDto(): TokenDto = TokenDto(
    access_token = null,
    refresh_token = null,
    token_type = null,
    scope = null,
    created_at = null,
)

fun TokenEntity.toTokenDto(): TokenDto {
    return TokenDto(
        access_token = accessToken,
        refresh_token = refreshToken,
        token_type = tokenType,
        scope = scope,
        created_at = createdAt,
    )
}

fun TokenDto.toTokenEntity(): TokenEntity {
    return TokenEntity().also { entity ->
        entity.accessToken = access_token
        entity.refreshToken = refresh_token
        entity.tokenType = token_type
        entity.scope = scope
        entity.createdAt = created_at
    }
}

fun TokenEntity.toToken(): Token {
    return Token(
        accessToken = accessToken,
        refreshToken = refreshToken,
        tokenType = tokenType,
        scope = scope,
        createdAt = createdAt
    )
}

fun Token.toTokenEntity(): TokenEntity {
    return TokenEntity().also { entity ->
        entity.accessToken = accessToken
        entity.refreshToken = refreshToken
        entity.tokenType = tokenType
        entity.scope = scope
        entity.createdAt = createdAt
    }
}