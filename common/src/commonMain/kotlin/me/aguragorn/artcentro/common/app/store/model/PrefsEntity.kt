package me.aguragorn.artcentro.common.app.store.model

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

class PrefsEntity : RealmObject {
    @PrimaryKey
    var id: String = ""
    var currentInstance: String? = null
    var accounts: RealmList<PrefsAccountEntity> = realmListOf()
    var activeAccount: PrefsAccountEntity? = null
}