package me.aguragorn.artcentro.common.timelines.domain.model

import io.ktor.http.*
import me.aguragorn.artcentro.common.util.unknownAccountUsername

data class AuthorModel(
    val displayName: String,
    val acctUrl: Url? = null,
    val staticAvatar: Url? = null,
) {
    companion object {
        val unknown = AuthorModel(
            displayName = unknownAccountUsername,
        )
    }
}