package me.aguragorn.artcentro.common.auth.domain.util

import me.aguragorn.artcentro.common.util.api.NetworkValues
import me.aguragorn.artcentro.common.util.api.queryParams
import java.net.URI

class OAuthValidator(
    private val networkVales: NetworkValues
) {
    fun isArtCentroOAuthRedirect(uri: URI?): Boolean {
        return uri != null
                && uri.scheme == networkVales.artcentroOAuthScheme
                && uri.host == networkVales.artcentroOAuthHost
                && uri.queryParams.containsKey("code")
    }
}