package me.aguragorn.artcentro.common.di

import me.aguragorn.artcentro.common.app.di.appDi
import me.aguragorn.artcentro.common.auth.di.authDi
import me.aguragorn.artcentro.common.timelines.di.timelineDi
import me.aguragorn.artcentro.common.util.di.utilsDi
import org.koin.dsl.module

val mainModule = module {
    utilsDi()
    appDi()
    authDi()
    timelineDi()
}
