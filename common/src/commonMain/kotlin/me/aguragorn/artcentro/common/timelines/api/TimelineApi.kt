package me.aguragorn.artcentro.common.timelines.api

import kotlinx.coroutines.withContext
import me.aguragorn.artcentro.common.timelines.domain.model.PostModel
import me.aguragorn.artcentro.common.util.Response
import me.aguragorn.artcentro.common.util.api.PixelFedClientFactory
import me.aguragorn.artcentro.common.util.ioContext
import me.aguragorn.artcentro.common.util.logger
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.timelines.api.getHomeTimeline
import me.aguragorn.pixelfed4kmpp.lib.timelines.model.HomeTimelineParams

class TimelineApi(
    private val pixelFedClientFactory: PixelFedClientFactory
) {
    private suspend fun pixelfedSecureClient(): PixelfedSecureClient {
        return pixelFedClientFactory.getSecureClient()
    }

    suspend fun getHomeTimelineStatuses(): Response<List<PostModel>> = withContext(ioContext) {
        val client = pixelfedSecureClient()

        return@withContext try {
            val posts = client
                .getHomeTimeline(HomeTimelineParams())
                .map { it.toPostModel() }

            Response.Success(posts)
        } catch (error: Exception) {
            log.e(error) { "Error while getting home timeline" }
            Response.Failure(error)
        }
    }

    companion object {
        private val log by logger()
    }
}