package me.aguragorn.artcentro.common.util.store

import android.content.Context

actual class StoreValues(context: Context) {
    actual val dbDir: String = "${context.filesDir}/db"
    actual val defaultDB: String = "default"
}