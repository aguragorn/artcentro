package me.aguragorn.artcentro.common.util.api

import android.util.Log
import me.aguragorn.artcentro.common.util.loggerWithName
import okhttp3.logging.HttpLoggingInterceptor

private val logger by loggerWithName("RequestLogs")

val loggingInterceptor = HttpLoggingInterceptor { Log.d("ac_ReuqestLogs", it) }
    .apply { level = HttpLoggingInterceptor.Level.BODY }
