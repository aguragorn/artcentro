package me.aguragorn.artcentro.common.util

import android.net.Uri
import java.net.URI

fun Uri.toURI(): URI {
    return URI.create(toString())
}

fun URI.toUri(): Uri {
    return Uri.parse(toString())
}