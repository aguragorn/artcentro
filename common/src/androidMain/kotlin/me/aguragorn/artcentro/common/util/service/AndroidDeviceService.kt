package me.aguragorn.artcentro.common.util.service

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import me.aguragorn.artcentro.common.util.toUri
import java.net.URI

class AndroidDeviceService(
    context: Context
) : DeviceService {
    private val appContext by lazy { context.applicationContext }

    override fun openBrowserTo(uri: URI) {
        try {
            CustomTabsIntent.Builder().build()
                .apply { intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) }
                .launchUrl(appContext, uri.toUri())

        } catch (e: Throwable) {
            appContext.startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(uri.toString())
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
            )
        }
    }
}