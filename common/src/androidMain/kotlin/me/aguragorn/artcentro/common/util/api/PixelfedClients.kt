package me.aguragorn.artcentro.common.util.api

import me.aguragorn.artcentro.common.BuildConfig
import me.aguragorn.pixelfed4kmpp.lib.PixelfedPublicClient
import me.aguragorn.pixelfed4kmpp.lib.PixelfedSecureClient
import me.aguragorn.pixelfed4kmpp.lib.authentication.store.SecretsStore

actual fun createPublicClient(
    instanceName: String,
    secretsStore: SecretsStore,
): PixelfedPublicClient {
    return PixelfedPublicClient(
        instanceName = instanceName,
        extension = {
            if (BuildConfig.DEBUG) {
                addNetworkInterceptor(loggingInterceptor)
            }
        },
    )
}

actual fun createSecureClient(
    instanceName: String,
    secretsStore: SecretsStore,
): PixelfedSecureClient {
    return PixelfedSecureClient(
        instanceName = instanceName,
        secretsStore = secretsStore,
        extension = {
            if (BuildConfig.DEBUG) {
                addNetworkInterceptor(loggingInterceptor)
            }
        },
    )
}