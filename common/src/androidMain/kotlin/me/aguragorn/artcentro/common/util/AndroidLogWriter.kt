package me.aguragorn.artcentro.common.util

import android.util.Log
import com.aguragorn.klog.LogWriter

object AndroidLogWriter: LogWriter {
    override fun d(tag: String, msg: () -> String, throwable: Throwable?) {
        Log.d(tag, msg(), throwable)
    }

    override fun e(tag: String, msg: () -> String, throwable: Throwable?) {
        Log.e(tag, msg(), throwable)
    }

    override fun i(tag: String, msg: () -> String, throwable: Throwable?) {
        Log.i(tag, msg(), throwable)
    }

    override fun w(tag: String, msg: () -> String, throwable: Throwable?) {
        Log.w(tag, msg(), throwable)
    }

    override fun wtf(tag: String, msg: () -> String, throwable: Throwable?) {
        Log.wtf(tag, msg(), throwable)
    }
}