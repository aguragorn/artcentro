package me.aguragorn.artcentro.common.di

import me.aguragorn.artcentro.common.util.api.NetworkValues
import me.aguragorn.artcentro.common.util.service.AndroidDeviceService
import me.aguragorn.artcentro.common.util.service.DeviceService
import me.aguragorn.artcentro.common.util.store.StoreValues
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val androidModule = module {
    singleOf(::AndroidDeviceService).bind<DeviceService>()
    singleOf(::NetworkValues)
    singleOf(::StoreValues)
}