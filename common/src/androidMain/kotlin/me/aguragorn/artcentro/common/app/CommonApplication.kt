package me.aguragorn.artcentro.common.app

import android.app.Application
import com.aguragorn.klog.Log
import com.aguragorn.klog.LogLevel
import me.aguragorn.artcentro.common.BuildConfig
import me.aguragorn.artcentro.common.di.androidModule
import me.aguragorn.artcentro.common.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

open class CommonApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Log.init(if (BuildConfig.DEBUG) LogLevel.DEBUG else LogLevel.INFO)

        startKoin {
            androidContext(this@CommonApplication)
            modules(androidModule, mainModule)
        }
    }
}