package me.aguragorn.artcentro.common.util.api

import android.content.Context
import me.aguragorn.artcentro.common.R

actual class NetworkValues(context: Context) {

    actual val artcentroOAuthUrl: String =
        "${context.getString(R.string.oauth_scheme)}://${context.getString(R.string.oauth_host)}"
    actual val artcentroOAuthScheme: String =
        context.getString(R.string.oauth_scheme)
    actual val artcentroOAuthHost: String =
        context.getString(R.string.oauth_host)
}