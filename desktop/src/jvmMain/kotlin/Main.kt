import androidx.compose.material.MaterialTheme
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import me.aguragorn.artcentro.common.app.ui.App
import me.aguragorn.artcentro.common.app.ui.AppView
import me.aguragorn.artcentro.common.auth.ui.AuthenticateView
import me.aguragorn.artcentro.common.di.desktopModule
import me.aguragorn.artcentro.common.di.mainModule
import me.aguragorn.artcentro.common.timelines.ui.TimelineView
import org.koin.core.context.startKoin
import org.koin.java.KoinJavaComponent.inject


fun main() = application {
    startKoin {
        modules(desktopModule, mainModule)
    }

    val appView: AppView by inject(AppView::class.java)
    val authenticateView: AuthenticateView by inject(AuthenticateView::class.java)
    val timelineView: TimelineView by inject(TimelineView::class.java)

    Window(onCloseRequest = ::exitApplication) {
        MaterialTheme {
            App(
                appView = appView,
                authenticateView = authenticateView,
                timelineView = timelineView
            )
        }
    }
}