package me.aguragorn.compose.widgets.image.utils

import android.graphics.BitmapFactory
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

actual object ImageLoader {
    private val cache = mutableMapOf<String, ImageBitmap>()

    actual fun getBitmapFrom(imageUrl: String): Flow<ImageBitmap?> = flow {
        val imageBitmap = cache[imageUrl] ?: HttpClient(OkHttp)
            .get(imageUrl)
            .readBytes()
            .let { BitmapFactory.decodeByteArray(it, 0, it.size) }
            .asImageBitmap()
            .also { cache[imageUrl] = it }

        emit(imageBitmap)
    }
}