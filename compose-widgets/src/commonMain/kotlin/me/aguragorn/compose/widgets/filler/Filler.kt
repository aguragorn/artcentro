package me.aguragorn.compose.widgets.filler

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun ColumnScope.Filler() {
    Spacer(Modifier.weight(weight = Float.MAX_VALUE, fill = true))
}

@Composable
fun RowScope.Filler() {
    Spacer(Modifier.weight(weight = Float.MAX_VALUE, fill = true))
}