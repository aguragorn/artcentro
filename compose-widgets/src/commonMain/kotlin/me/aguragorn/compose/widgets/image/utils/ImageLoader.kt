package me.aguragorn.compose.widgets.image.utils

import androidx.compose.ui.graphics.ImageBitmap
import kotlinx.coroutines.flow.Flow

expect object ImageLoader {
    fun getBitmapFrom(imageUrl: String): Flow<ImageBitmap?>
}

