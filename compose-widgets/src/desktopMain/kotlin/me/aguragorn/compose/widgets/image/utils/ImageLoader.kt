package me.aguragorn.compose.widgets.image.utils

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.jetbrains.skia.Image

actual object ImageLoader {
    private val cache = mutableMapOf<String, ImageBitmap>()

    actual fun getBitmapFrom(imageUrl: String): Flow<ImageBitmap?> = flow {
        val imageBitmap = cache[imageUrl] ?: HttpClient(OkHttp)
            .get(imageUrl)
            .readBytes()
            .let { Image.makeFromEncoded(it) }
            .toComposeImageBitmap()
            .also { cache[imageUrl] = it }

        emit(imageBitmap)
    }
}